node 'yourhostname' {
class { 'owncloud6':
        mysqlroot_password => 'mysqlrootpassword',
        db_password        => 'owncloudpassword',
        db_host            => 'localhost',
        db_name            => 'owncloud',
        db_user            => 'ownclouduser', }
}
