This is a puppet module to automate owncloud 6 installation on wheezy

/!\ warning this will modify your apache/mysql config. Better use a clean debian wheezy.

"Installation guide:"

sudo apt-get update

sudo apt-get install -y puppet git

cd ~

git clone https://bitbucket.org/heyjack/owncloud6.git

"edit hostname, passwords before launching:"

vi ~/owncloud6/manifests/nodes.pp

launch puppet:

sh ~/owncloud6/papply.sh

"wait few minutes (libreoffice can take quite a while to install) then go to:"

https://myhostname/

"fill login/passwd, click advanced

select MySQL and fill informations"

ENJOY!!

