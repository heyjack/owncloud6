#!/bin/sh
PUPPETDIR=$HOME/owncloud6
sudo /usr/bin/puppet apply --modulepath ${PUPPETDIR}/modules ${PUPPETDIR}/manifests/site.pp --no-report
