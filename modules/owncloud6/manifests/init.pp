#owncloud 6 install
##needed modules: puppetlabs/apt, puppetlabs/mysql
class owncloud6 (
  $mysqlroot_password = 'UNSET',
  $db_password = 'UNSET',
  $db_host = 'UNSET',
  $db_name = 'UNSET',
  $db_user = 'UNSET', ) {

##Default values
$_mysqlroot_password = $mysqlroot_password ? {
  'UNSET' => 'mysqlrootpassword',
  default => $mysqlroot_password,
}

$_db_password = $db_password ? {
  'UNSET' => 'owncloudpassword',
  default => $db_password,
}

$_db_host = $db_host ? {
  'UNSET' => 'localhost',
  default => $db_host,
}

$_db_name = $db_name ? {
  'UNSET' => 'owncloud',
  default => $db_name,
}

$_db_user = $db_user ? {
  'UNSET' => 'ownclouduser',
  default => $db_user,
}


##Owncloud
apt::source { 'owncloud':
  location          => 'http://download.opensuse.org/repositories/isv:ownCloud:community/Debian_7.0/',
  release           => '/',
  repos             => '',
  include_src       => false,
  required_packages => 'debian-keyring debian-archive-keyring',
  key_source        => 'http://download.opensuse.org/repositories/isv:/ownCloud:/community/Debian_7.0/Release.key',
  key               => 'BA684223',
}

package { 'owncloud':
  ensure   => installed,
  require  => Apt::Source ['owncloud'],
}

##Mysql
class { '::mysql::server':
  root_password    => $mysqlroot_password,
  require          => Package['owncloud'],
}

mysql::db { $db_name:
  user     => $db_user,
  password => $db_password,
  host     => $db_host,
  grant    => ['ALL'],
  require  => Package ['mysql-server'],
}

##Apache2
package { 'apache2':
  ensure  => installed,
  require => Package['owncloud'],
}

package { 'libapache2-mod-php5':
  ensure  => installed,
  notify  => Service ['apache2'],
  require => Package['apache2'],
}

service { 'apache2':
  ensure     => running,
  enable     => true,
  require    => Package['apache2'],
  hasstatus  => true,
  hasrestart => true,
}

file { '/etc/apache2/sites-available/owncloud6':
  owner   => root,
  mode    => '0640',
  source  => 'puppet:///modules/owncloud6/owncloud6',
  require => Package['apache2'],
  notify  => Service ['apache2'],
}

#disable port 80
file { '/etc/apache2/ports.conf':
  owner   => root,
  mode    => '0644',
  source  => 'puppet:///modules/owncloud6/ports.conf',
  require => Package['apache2'],
  notify  => Service ['apache2'],
}

exec { '/usr/sbin/a2enmod ssl':
  unless  => '/bin/readlink -e /etc/apache2/mods-enabled/ssl.load',
  notify  => Service ['apache2'],
  require => Package['apache2'],
}

exec { '/usr/sbin/a2dissite default-ssl':
  onlyif  => '/bin/readlink -e /etc/apache2/sites-enabled/default-ssl',
  notify  => Service ['apache2'],
  require => Package['apache2'],
}

exec { '/usr/sbin/a2dissite default':
  onlyif  => '/bin/readlink -e /etc/apache2/sites-enabled/000-default',
  notify  => Service ['apache2'],
  require => Package['apache2'],
}

exec { '/usr/sbin/a2ensite owncloud6':
  unless  => '/bin/readlink -e /etc/apache2/sites-enabled/owncloud6',
  notify  => Service ['apache2'],
  require => Package['apache2'],
}


package { 'libreoffice':
  ensure  => installed,
  require => Package['owncloud'],
}

##extra
  $extra = [
  'openssl',
  'php5-ldap',
  'php5-xcache',
  'php5-imagick',
  'smbclient',
  'ffmpeg',
  ]
package { $extra:
  ensure  => installed,
  require => Package['owncloud'],
}

}
